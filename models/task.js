const mongoose = require ("mongoose");
//Create the schema and model
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Export the File
module.exports = mongoose.model("Task", taskSchema);
